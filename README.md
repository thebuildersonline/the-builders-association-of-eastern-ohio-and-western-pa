The Builders Association of Eastern Ohio and Western PA


The Builders represent the favored skilled union construction manpower choice in the Ohio counties of Trumbull, Mahoning and Columbiana and the Pennsylvania counties of Mercer and Lawrence.


Address: 1372 Youngstown Kingsville Rd SE, Vienna, OH 44473, USA


Phone: 330-539-6050


Website: http://www.thebuildersonline.com/
